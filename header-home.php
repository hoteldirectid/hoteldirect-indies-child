<!DOCTYPE html>
<html lang="en">
<head>
<?php wp_head(); ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
	<header>
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="logo pull-left">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/indies-logo.png" alt="">
      </div>
      <div class="navbar-header">
        <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button> -->
        <button type="button" class="navbar-toggle" onclick="openNav()">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse pull-center">
        <div class="row">
          <div class="col-md-2 ">
            <div class="head-logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/indies-logo.png" alt="">
            </div>
          </div>
          <div class="col-md-8 ">
            <?php karisma_nav(); ?>
            </div>
          <div class="col-md-2 ">
            <div class="head-logo">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/accola2.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <!-- mobile menu -->
      <div class="mobmenu">
        <div id="mobinav" class="sidenav">
          <div class="mobinav-inner login">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
              <?php karisma_nav_mobile(); ?>
          </div>
        </div><!-- end .mobinav -->
        <span class="icon-list-add mobico" onclick="openNav()"></span>
      </div><!-- end .mobmenu -->
    </div>
  </div>
</header>