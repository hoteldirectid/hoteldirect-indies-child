<?php get_header(); ?>

	<div id="wrapper" class="homepage">
		<div class="section main-slider">
			<div id="slider-main" class="owl-carousel">
				<div class="owl-slide" style="background-image: url('asset/img/pool.jpg')"></div>
				<div class="owl-slide" style="background-image: url('asset/img/roomsa.jpg')"></div>
			</div>
			<!-- end .slider-main -->

			<div class="container">
				<div class="menu-home">
					<div class="col-md-3 col-sm-6 col-xs-6 menu">
						<a href="#">
							<div class="menu-home-content">
								<img src="asset/img/galery.svg" width="24" height="24">
								<h4>GALLERY</h4>
							</div>
						</a>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 menu">
						<a href="#">
							<div class="menu-home-content">
								<img src="asset/img/travel-agent.svg" width="24" height="24">
								<h4>TRAVEL AGENT</h4>
							</div>
						</a>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 menu">
						<a href="#">
							<div class="menu-home-content">
								<img src="asset/img/restaurant.svg" width="24" height="24">
								<h4>RESTAURANT</h4>
							</div>
						</a>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 menu">
						<a href="#">
							<div class="menu-home-content">
								<img src="asset/img/promotion.svg" width="24" height="24">
								<h4>PROMOTIONS</h4>
							</div>
						</a>
					</div>
				</div>
			</div>
			<!-- end container -->
		</div>
		<!-- end .main-slider -->

		<div class="section section__block content-intro">
			<div class="container">
				<div class="sectitle">
					<h2>INDIES HERITAGE HOTEL</h2>
				</div>
				<div class="text-center">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
						dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				</div>
			</div>
			<!-- end .container -->
		</div>
		<!-- end .content-intro -->

		<div class="section section__block content-gallery">
			<div class="container">
				<div class="sectitle sectitle__seconda">
					<h2>OUR GALERY</h2>
					<span class="subtext">Our Favorite Product</span>
				</div>
				<div class="outer-gallery">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="inner-gallery">
								<div class="overlay">
									<div class="overlay-line">
										<a href="page-gallery-details.html">
											<h4>SEE DETAIL</h4>
										</a>
									</div>
								</div>
								<div class="gallery-img">
									<img src="asset/img/prd.jpg" alt="">
								</div>
								<div class="gallery-desc">
									<div class="gallery-desctittle">
										<h5>PRODUCT I</h5>
										<h4>IDR 100.000</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="inner-gallery">
								<div class="overlay">
									<div class="overlay-line">
										<a href="page-gallery-details.html">
											<h4>SEE DETAIL</h4>
										</a>
									</div>
								</div>
								<div class="gallery-img">
									<img src="asset/img/prd2.jpg" alt="">
								</div>
								<div class="gallery-desc">
									<div class="gallery-desctittle">
										<h5>PRODUCT II</h5>
										<h4>IDR 100.000</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="inner-gallery">
								<div class="overlay">
									<div class="overlay-line">
										<a href="page-gallery-details.html">
											<h4>SEE DETAIL</h4>
										</a>
									</div>
								</div>
								<div class="gallery-img">
									<img src="asset/img/prd3.jpg" alt="">
								</div>
								<div class="gallery-desc">
									<div class="gallery-desctittle">
										<h5>PRODUCT III</h5>
										<h4>IDR 100.000</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="textlink">
					<a href="#">view all product</a>
				</div>
			</div>
		</div>

		<div class="section section__block content-testimonial">
			<div class="container">
				<div class="sectitle">
					<h2>TESTIMONIAL</h2>
				</div>
				<div class="testimonial-slide owl-carousel owl-theme">
					<div class="title-slide">
						<div class="text-center">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
								dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						</div>
						<a>Aka</a>
					</div>
					<div class="title-slide">
						<div class="text-center">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
								dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						</div>
						<a>Roger</a>
					</div>
					<div class="title-slide">
						<div class="text-center">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
								dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						</div>
						<a>Nata</a>
					</div>
				</div>
			</div>
		</div>
		<!-- end .content-intro -->
	</div>
	<!-- end #wrapper -->

<?php get_footer(); ?>