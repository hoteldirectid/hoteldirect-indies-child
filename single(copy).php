<?php get_header(); ?>

	<div id="wrapper" class="singlepage">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php $background = get_field('background');
		// print_r($background);die();
		?>
		<div class="coverbox" style="background-image:url(<?php echo $background['url'];?>)">
			<div class="boxtitle">
				<h1><?php the_sub_field('title');?></h1>
				<span class="subtext"><?php the_sub_field('description');?></span>
			</div>
		</div>
		<!-- end .coverbox -->

		<div class="section ctnsingle-detail">
			<div class="container">
				<div class="sectitle__maintittle">
					<h1><?php the_title();?></h1>
				</div>

				<!-- slider -->
				<div class="row">
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="contentslide">
							<div class="slider-for">
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining1.jpg" alt="image" />
	                </span>
								</div>
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining2.jpg" alt="image"/>
	                </span>
								</div>
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining3.jpg" alt="image"/>
	                </span>
								</div>
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/4.jpg" alt="image"/>
	                </span>
								</div>
							</div>
						</div>

						<div class="contentslideinfo">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget felis ullamcorper, pharetra magna ac, tempus ante. Quisque ac sem vel nisi finibus luctus nec at erat. Morbi at ante vel est sodales ultrices. Aenean vitae malesuada leo, vitae</p>
						</div>
					</div>

					<div class="col-md-4 col-sm-4 hidden-xs">
						<div class="contentslidethumb">
							<div class="slider-nav">
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining1.jpg" alt="image" draggable="false"/>
	                </span>
								</div>
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining2.jpg" alt="image" draggable="false"/>
	                </span>
								</div>
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining3.jpg" alt="image" draggable="false"/>
	                </span>
								</div>
								<div class="item">
									<span>
	                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining4.jpg" alt="image" draggable="false"/>
	                </span>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="section content-dining ">
			<div class="sectitle sectitle__others">
				<h2>OTHERS DINING</h2>
			</div>
			<div class="container">
				<div class="outerlistcontent">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining1.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Dessert</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining2.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Main Course</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining3.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Appertizer</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining4.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Breakfast</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>
					</div>
					<!-- end .row -->
				</div>
				<!-- end .outerlistcontent -->
			</div>
			<!-- end .container -->
		</div>
		<!-- end content -->
		<?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
	</div>
	<!-- end wrapper -->

  <?php get_footer(); ?>