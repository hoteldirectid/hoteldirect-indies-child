	<footer class="main-footer">
	<div class="footer-link text-center">
		<div class="logo-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="logo-footerinner">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/indies-logo.png" alt="">
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="logo-footerinner">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/fabuho.png" alt="">
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="logo-footerinner">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/logo-accola.png" alt="">
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="logo-footerinner">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/logo-zest.png" alt="">
						</div>
					</div>
				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
		</div>
		<div class="social-footer text-center">
			<p>Stay In Touch with Us :</p>
			<i class="fa fa-facebook-official" aria-hidden="true"></i>
			<ul class="text-center">
				<li><a href="#" class="icn-facebook"><i class="fab fa-facebook-f"></i></a></li>
				<li><a href="#" class="icn-twitter"><i class="fab fa-twitter"></i></a></li>
				<li><a href="#" class="icn-instagram"><i class="fab fa-instagram"></i></a></li>
			</ul>
		</div>
	</div>
	<!-- end  .footer -->
	<div class="footer-info">
		<div class="container">
			<p class="verticent-inner"><?php krs_footer(); ?></p>
		</div>
	</div>
	<!-- end .footer-info -->
</footer>
<?php wp_footer(); ?>
</body>

</html>
