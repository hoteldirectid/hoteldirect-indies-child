<?php get_header(); ?>

	<div id="wrapper" class="singlepage">
		<?php
		if (have_posts()): while (have_posts()) : the_post();

		 get_template_part('partials', 'loader');
		?>


		<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	</div>
	<!-- end #wrapper -->

  <?php get_footer(); ?>
