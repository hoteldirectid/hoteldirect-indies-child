<?php get_header(); ?>

	<div id="wrapper" class="singlepage">
        <div class="section home-contact">
            <div class="container">		
                <?php
                if (have_posts()): while (have_posts()) : the_post();
                
                the_content();
                ?>
                

                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
	</div>
	<!-- end #wrapper -->

  <?php get_footer(); ?>