    <div class="section contact">
      <div class="container">
        <div class="title-section">
          <h2><?php the_sub_field('title'); ?></h2>
          <h3><?php the_sub_field('title_2'); ?></h3>
        </div>
        <div class="section form-contact">
          <div class="row">
              <div class="col-sm-5">
                <div class="contact-address">

                <?php $details = get_sub_field('details');

                foreach ($details as $detail) :
                ?>
                  <div class="contact-item">
                    <div class="box-icon">
                      <div class="icon-inner">
                        <span class="icon">
                          <i class="<?php echo $detail['icon']; ?>"></i>
                        </span>
                      </div>
                    </div>
                    <p><?php echo $detail['description']; ?></p>
                  </div><!-- end .contact-item -->
                <?php endforeach ?>
                <?php wp_reset_postdata(); ?>

                </div><!-- end .contact-address -->
              </div>
              <div class="col-sm-7">
                <?php the_sub_field('form'); ?>
                <?php /*
                <div class="contact-form">
                  <form action="" method="post" class="">
                    <div class="row">
                      <div class="col-sm-6"><span class="yname"><input type="text" name="your-name" value="" size="40" class="form-control" aria-invalid="false" placeholder="Full Name"></span></div>
                      <div class="col-sm-6"><span class="yemail"><input type="email" name="your-email" value="" size="40" class="form-control" aria-required="true" aria-invalid="false" placeholder="Your Email"></span></div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12"><span class="ysubject"><input type="text" name="your-subject" value="" size="40" class="form-control" aria-invalid="false" placeholder="Subject"></span></div>
                    </div>
                    <div class="row">
                      <div class="col-sm-12"><span class="ymessage"><textarea name="your-message" cols="40" rows="10" class="form-control" aria-invalid="false" placeholder="Your Message"></textarea></span></div>
                    </div>
                    <p><input type="submit" value="Send Message" class="btn btn-main large"></p>
                  </form>
                </div>
               */ ?>
              </div>
            </div>
        </div>
      </div><!-- end .container -->
      <div class="section map">
        <?php

        $location = get_sub_field('maps');

        if( !empty($location) ):
        ?>
        <div class="acf-map">

            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
        </div>
        <?php endif; ?>
      </div>
    </div><!-- end .content-intro -->
