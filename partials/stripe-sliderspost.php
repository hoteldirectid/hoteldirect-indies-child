<!-- roomdetail -->
<div class="section ctnsingle-detail">
	<div class="container">
		<div class="sectitle__maintittle">
			<h1><?php the_title();?></h1>
		</div>
		<div class="outer-ctnsingle-detail">
			<div class="row">
				<div class="col-md-8">
					<div class="main-slider">
						<?php $images = get_sub_field('slider'); if($images): ?> 
						<div id="slider-main" class="owl-carousel">
							<?php foreach ($images as $image) {
									echo '<div class="owl-slide" style="background-image: url(\''.$image['url'].'\')"></div>';
								}
							?>
						</div>
						<!-- end .slider-main -->
						<?php endif; ?>
					</div>
					<!-- end .main-slider -->
					<div class="ctnsingle-text">
						<?php the_sub_field('description'); ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="ctnsingle-text">
						<?php $widget = get_sub_field('widget'); ?>
						<h4><?php echo $widget['title']; ?></h4>
						<?php if($widget['content']) :?>
						<ul>
							<?php foreach ($widget['content'] as $value) {
								echo '<li>'.$value['list'].'</li>';
								}
							?>
						</ul>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-12">
					<div class="ctnsingle-btn">
						<button type="submit" class="btn btn-main large">BOOK</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
