<?php
$images = get_field('sliders');
if($images): ?>
    <div class="section main-slider">

      <?php do_shortcode("[booking_engine]"); ?>

      <div id="slider-main" class="owl-carousel">
        <?php
            foreach ($images as $image) {
                echo '<div class="owl-slide" style="background-image: url(\''.$image['url'].'\')"></div>';
            }
        ?>
	    </div><!-- end .slider-main -->
      
  		<?php if(is_front_page()) : ?>
  			<div class="container">
  				<div class="menu-home">
  					<div class="col-md-3 col-sm-6 col-xs-6 menu">
  						<a href="#">
  							<div class="menu-home-content">
  								<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/galery.svg" width="24" height="24">
  								<h4>GALLERY</h4>
  							</div>
  						</a>
  					</div>
  					<div class="col-md-3 col-sm-6 col-xs-6 menu">
  						<a href="#">
  							<div class="menu-home-content">
  								<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/travel-agent.svg" width="24" height="24">
  								<h4>TRAVEL AGENT</h4>
  							</div>
  						</a>
  					</div>
  					<div class="col-md-3 col-sm-6 col-xs-6 menu">
  						<a href="#">
  							<div class="menu-home-content">
  								<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/restaurant.svg" width="24" height="24">
  								<h4>RESTAURANT</h4>
  							</div>
  						</a>
  					</div>
  					<div class="col-md-3 col-sm-6 col-xs-6 menu">
  						<a href="#">
  							<div class="menu-home-content">
  								<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/promotion.svg" width="24" height="24">
  								<h4>PROMOTIONS</h4>
  							</div>
  						</a>
  					</div>
  				</div>
  			</div>
  		<?php endif; ?>

    </div><!-- end .main-slider -->
<?php endif; ?>
