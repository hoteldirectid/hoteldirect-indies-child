		<div class="section map">
			<div class="container">
                <?php

                $location = get_sub_field('maps');

                if( !empty($location) ):
                ?>
                <div class="acf-map">

                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
                <?php endif; ?>
			</div>
		</div>

		<div class="section form-contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contact-desc">
							<p><?php the_sub_field('address'); ?></p>
						</div>

					</div>
					<div class="col-sm-6">
						<div class="contact-form">
							<?php echo do_shortcode('[contact-form-7 id="6943" title="ctc1"]') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
