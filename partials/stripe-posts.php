		<!-- roomlist -->
		<div class="section content-dining">
			<div class="sectitle sectitle__others">
				<h2><?php the_sub_field('title'); ?></h2>
			</div>
			<div class="container">
				<div class="outerlistcontent">
					<div class="row">
                        <?php $posts = get_sub_field('post');
                        foreach ($posts as $post) : 
                        ?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="<?php the_permalink(); ?>">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
                                        <?php 
                                        if ( has_post_thumbnail() ) {
											$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
											echo '<img src="'.$image[0].'" data-id="'.$post->ID.'">';
										}
                                        ?>
									</div>
									<!-- end .ctnimage -->
									<div class="ctndesc">
										<div class="ctntitle">
											<h4><?php the_title(); ?></h4>
										</div>
									</div>
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>
                        <?php endforeach ?>
                        <?php wp_reset_postdata(); ?>
					</div>
					<!-- end .row -->
				</div>
				<!-- end .outerlistcontent -->
			</div>
		</div>