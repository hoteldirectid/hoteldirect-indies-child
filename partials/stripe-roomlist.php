		<!-- roomlist -->
		<div class="section content-rooms">
			<div class="container">
				<div class="sectitle sectitle__others">
					<h2>OTHERS ROOM</h2>
				</div>
				<div class="outerlistcontent">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="#">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/1.jpg" alt="">
										<div class="icofac">
											<ul class="clearfix">
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/wifi.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/tv.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/roti.svg" alt=""></li>
											</ul>
										</div>
									</div>
									<!-- end .ctnimage -->
									<div class="ctndesc">
										<div class="ctntitle">
											<h4>SUITE ROOM</h4>
										</div>
									</div>

								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="#">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/1.jpg" alt="">
										<div class="icofac">
											<ul class="clearfix">
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/wifi.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/tv.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/roti.svg" alt=""></li>
											</ul>
										</div>
									</div>
									<!-- end .ctnimage -->
									<div class="ctndesc">
										<div class="ctntitle">
											<h4>DELUXE ROOM</h4>
										</div>
									</div>
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="#">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/slide/1.jpg" alt="">
										<div class="icofac">
											<ul class="clearfix">
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/wifi.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/tv.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/roti.svg" alt=""></li>
											</ul>
										</div>
									</div>
									<!-- end .ctnimage -->
									<div class="ctndesc">
										<div class="ctntitle">
											<h4>BALCONY ROOM</h4>
										</div>
									</div>
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>
					</div>
					<!-- end .row -->
				</div>
				<!-- end .outerlistcontent -->
			</div>
		</div>