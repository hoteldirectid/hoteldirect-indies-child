<div class="section section__block content-intro">
    <div class="container">
        <div class="sectitle">
            <h2><?php the_sub_field('title'); ?></h2>
        </div>
        <div class="text-center">
            <p><?php the_sub_field('description'); ?></p>
        </div>
    </div>
    <!-- end .container -->
</div>
<!-- end .content-intro -->