<div class="section section__block content-testimonial">
			<div class="container">
				<div class="sectitle">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
				<div class="testimonial-slide owl-carousel owl-theme">
          <?php
            $args = array(
              'post_type' => 'hotel-info',
              'category_name' => 'testimonial'
              );
            query_posts($args);
            
            if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div class="title-slide">
              <div class="text-center">
                <?php the_content(); ?>
              </div>
              <a><?php the_title(); ?></a>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
		<!-- end .content-intro -->