<div class="section ctnsingle-detail">
    <div class="container">
        <div class="sectitle__maintittle">
            <h1><?php the_title();?></h1>
        </div>
        <!-- slider -->
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="contentslide">
                    <div class="slider-for">
                        <?php $images = get_sub_field('slider'); 
                        if($images): 
                            foreach ($images as $image) :
                                echo '<div class="item"><span>';
                                echo '<img src="'.$image['url'].'" alt="image" />';
                                echo '</span></div>';
                            endforeach; 
                        endif; ?>
                    </div>
                </div>

                <div class="contentslideinfo">
                    <?php the_sub_field('description'); ?>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="contentslidethumb">
                    <div class="slider-nav">
                        <?php 
                        if($images): 
                            foreach ($images as $image) :
                                echo '<div class="item"><span>';
                                echo '<img src="'.$image['url'].'" alt="image" draggable="false" />';
                                echo '</span></div>';
                            endforeach; 
                        endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="ctnsingle-btn">
                    <form method="POST" action="<?php echo site_url() .'/cart/?add-to-cart='.get_the_ID(); ?>">
                    <button type="submit" name="add-to-cart" value="" class="single_add_to_cart_button btn btn-main large">
                    Buy
                    </button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>