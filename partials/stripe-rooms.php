		<!-- roomlist -->
		<div class="section content-rooms">
			<div class="container">
				<div class="outerlistcontent">
					<div class="row">
                        <?php $posts = get_sub_field('room_list');
                        foreach ($posts as $post) : 
                        ?>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="<?php the_permalink(); ?>">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
                                        <?php 
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail();
                                        } 
                                        ?>
                        				<div class="icofac">
											<ul class="clearfix">
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/wifi.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/tv.svg" alt=""></li>
												<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/ico/roti.svg" alt=""></li>
											</ul>
										</div>
									</div>
									<!-- end .ctnimage -->
									<div class="ctndesc">
										<div class="ctntitle">
											<h4><?php the_title(); ?></h4>
										</div>
									</div>
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>
                        <?php endforeach ?>
                        <?php wp_reset_postdata(); ?>
					</div>
					<!-- end .row -->
				</div>
				<!-- end .outerlistcontent -->
			</div>
		</div>