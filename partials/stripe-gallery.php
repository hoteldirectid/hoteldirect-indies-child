<div class="section section__block content-gallery">
    <div class="container">
        <?php if(!empty(get_sub_field('title'))) : ?>
        <div class="sectitle sectitle__seconda">
            <h2><?php the_sub_field('title'); ?></h2>
            <span class="subtext"><?php the_sub_field('description'); ?></span>
        </div>
        <?php endif; ?>
        <div class="outer-gallery">
            <div class="row">
                <?php  $posts = get_sub_field('gallery');
                if($posts) :
                    foreach ($posts as $post) : 
                        $product = wc_get_product($post->ID );
                        // $product->get_regular_price();
                        // $product->get_sale_price();
                        // $product->get_price();

                    ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-gallery">
                            <div class="overlay">
                                <div class="overlay-line">
                                    <a href="<?php the_permalink(); ?>">
                                        <h4>SEE DETAIL</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="gallery-img">
                            <?php 
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail();
                            } 
                            ?>
                            </div>
                            <div class="gallery-desc">
                                <div class="gallery-desctittle">
                                    <h5><?php the_title(); ?></h5>
                                    <h4><?php echo $product->get_price(); ?></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <div class="textlink">
            <a href="<?php home_url();?>/indies-heritage/gallery/">view all product</a>
        </div>
    </div>
</div>