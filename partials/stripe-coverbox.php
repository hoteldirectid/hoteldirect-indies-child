<?php $background = get_sub_field('background');
?>
<div class="coverbox" style="background-image:url(<?php echo $background['url'];?>)">
    <div class="boxtitle">
        <h1><?php the_sub_field('title');?></h1>
        <span class="subtext"><?php the_sub_field('description');?></span>
    </div>
</div>
<!-- end .coverbox -->

