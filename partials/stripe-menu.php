		<div class="section content-dining ">
			<div class="container">
				<div class="outerlistcontent">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining1.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Dessert</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining2.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Main Course</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining3.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Appertizer</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="innerlistcontent__seconda">
								<a href="page-dining-details.html">
									<div class="ctnimage">
										<div class="bxoverlay"></div>
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/img/dining4.jpg" alt="">
									</div>
									<!-- end .ctnimage -->

									<div class="ctndesc">
										<div class="ctntitle">
											<h4>Breakfast</h4>
										</div>
									</div>
									<!-- end .ctndesc -->
								</a>
							</div>
							<!-- end .innerlistcontent -->
						</div>
					</div>
					<!-- end .row -->
				</div>
				<!-- end .outerlistcontent -->
			</div>
			<!-- end .container -->
		</div>
		<!-- end content -->