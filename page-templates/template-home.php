<?php /* Template Name: Home Template */ 
if(is_front_page()) : 
    get_header('home'); 
    $classwrapper = 'homepage';
else:  
    get_header();
    $classwrapper = 'singlepage';
 endif; ?>

  <div id="wrapper" class="<?php echo $classwrapper; ?>">

      <?php

        if (have_posts()) : while (have_posts()) : the_post();

            get_template_part('partials', 'loader');
          
        endwhile; endif; // close the WordPress loop
        ?>

  </div><!-- end #wrapper -->
  <?php get_footer(); ?>